#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Crystallize quaternions
#
#

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt


def normalize(v):
    return v / np.sqrt(np.sum(v**2))


def normalize_all(quaternions):
    return quaternions / np.sqrt(np.sum(quaternions**2, axis=1))[:, None]


def shoemake(coordinates):
    """Shoemake transformation."""
    # http://planning.cs.uiuc.edu/node198.html
    coordinates = np.atleast_2d(coordinates)
    t1 = np.sqrt(1. - coordinates[:,0])
    t2 = np.sqrt(coordinates[:,0])
    s1 = 2. * np.pi * coordinates[:,1]
    s2 = 2. * np.pi * coordinates[:,2]
    return np.dstack((t1 * np.sin(s1), t1 * np.cos(s1), 
                      t2 * np.sin(s2), t2 * np.cos(s2)))[0]


def random_quaternion(n=1):
    u = np.random.random(size=(n, 3))
    return shoemake(u)


def random_axis():
    x = np.random.normal(size=3)
    return normalize(x)


def axisangle_to_q(v, theta):
    v = np.array(v)
    v = normalize(v)
    x, y, z = v
    theta /= 2.
    w = np.cos(theta)
    x = x * np.sin(theta)
    y = y * np.sin(theta)
    z = z * np.sin(theta)
    return np.array([w, x, y, z])


def q_mult(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    return np.array([w, x, y, z])


def angle_vector(v1, v2):
    length1 = np.sqrt(np.dot(v1, v1))
    length2 = np.sqrt(np.dot(v2, v2))
    return np.arccos(np.dot(v1, v2) / (length1 * length2))


def quaternion_to_shoemake(wxyz, wrap=True, verbose=True):
    """ 
        graphical solution:
          - y and z define a point within a 2D circle of radius 1.0, where
            sqrt(u1) is the distance from (0,0,0). Therefore, u1 is
            y**2 + z**2.
    """

    if abs(np.sum(np.array(wxyz)**2) - 1.0) > 1e-15:
        raise RuntimeError('input quaternion is not normalized')

    w, x, y, z = wxyz

    u1 = y**2 + z**2
    u1_ = -(w**2 + x**2) + 1

    if abs(u1 - u1_) > 1e-15:
        raise RuntimeError('expected u1 to be equal to u1_')

    if u1 == 0.:
        if verbose:
            print "WARNING: numerical singularity in quaternion_to_cube3"
            print "WARNING: impossible to determine u3"

    if u1 == 1.:
        if verbose:
            print "WARNING: numerical singularity in quaternion_to_cube3"
            print "WARNING: impossible to determine u2"

    # arctan2 returns values [-pi, +pi]
    u2 = np.arctan2(w,x)
    u3 = np.arctan2(y,z)

    if not wrap:
        return u1, u2, u3

    if u2 < 0.:
        u2 += 2.0 * np.pi
    if u3 < 0.:
        u3 += 2.0 * np.pi

    return u1, u2, u3


def plot_quaternions_distribution(quaternions, verbose=True):
    u1 = []
    u2 = []
    u3 = []

    for q in quaternions:
        a, b, c = quaternion_to_shoemake(q, verbose=verbose)
        u1.append(a)
        u2.append(b)
        u3.append(c)

    ax = plt.gca(projection='3d')
    ax.plot(u1, u2, u3, 'ok')
    plt.show()
