#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Crystallize quaternions
#
#

import numpy as np

import utils

class Quaternion_sampling():
    
    def __init__(self, n_iter=1000, learning_rate=1.0, perturbation_scale=0.1, energy_update=100, verbose=False):
        """Initialize a Quaternion_sampling object.

        Args:
            n_iter (int): number of iteration (default: 1000)
            learning_rate (float): The initial learning rate for the optimization (default: 1.0)
            perturbation_scale (float): The pertubation movement (default: 0.1)
            energy_update (int): energy frequency update of the all system (default: 100)
            verbose (bool): print intermediate energy step

        """
        self._n_iter = n_iter
        self._learning_rate = learning_rate
        self._perturbation_scale = perturbation_scale
        self._energy_update = energy_update
        self._verbose = verbose
        
    def _potential(self, angle_radians, x0=0.0, sigma=0.4):
        return max(0.0, 1.0 - angle_radians / sigma)

    def _eval_all(self, quaternions):
        energy = 0.0
        
        for i in xrange(quaternions.shape[0]):
            energy += self._eval_one(quaternions[i], i, quaternions[i+1:])
        
        return energy

    def _eval_one(self, q, q_idx, quaternions):
        energy = 0.0
        
        for i in xrange(quaternions.shape[0]):
            if i != q_idx:
                angle = utils.angle_vector(q, quaternions[i])
                energy += self._potential(angle)
        
        return energy
    
    def fit_transform(self, data):
        """Run the Monte-Carlo optimization.
        
        Args:
            data (int or ndarray): Either the number of quaternions or a set of predefined quaternions
        
        Returns:
            ndarray: optimized quaternions
            
        """
        if isinstance(data, int):
            quaternions = utils.random_quaternion(data)
        elif isinstance(data, np.ndarray):
            assert data.shape[1] == 4, "Error: not quaternions, dimension != 4"
            quaternions = data
        
        alpha = self._learning_rate / float(self._n_iter)
        n_quaternions = quaternions.shape[0]
        
        # monte carlo
        for i in xrange(self._n_iter):
            if i % self._energy_update == 0:
                quaternions = utils.normalize_all(quaternions)
                total_energy = self._eval_all(quaternions)
                
                if self._verbose:
                    print "iteration number %10d --- total energy: %15.6f" % (i, total_energy)
                
                # Termination criteria
                if total_energy == 0:
                    break

            # random perturbation
            rotaxis = utils.random_axis()
            rotangle = self._learning_rate * self._perturbation_scale * np.random.normal()
            q2 = utils.axisangle_to_q(rotaxis, rotangle)

            # choose one quaternion randomly
            q_idx = np.random.choice(n_quaternions)
            q1 = quaternions[q_idx]

            # evaluate energy involving the chosen quaternion
            e0 = self._eval_one(q1, q_idx, quaternions)

            # evaluate energy change
            new_q1 = utils.q_mult(q1, q2)
            e1 = self._eval_one(new_q1, q_idx, quaternions)

            # replace by metropolis criterion
            if e1 < e0:
                quaternions[q_idx] = new_q1

            self._learning_rate -= alpha
        
        # It means that we didn't succeed to remove all the clashes
        if total_energy >= 0:
            total_energy = self._eval_all(quaternions)
            quaternions = utils.normalize_all(quaternions)
        
        self._final_energy = total_energy
        self._final_n_iter = i
        
        return quaternions


def main():
    qs = Quaternion_sampling(n_iter=2000, verbose=True)
    quaternions = qs.fit_transform(50)
    print "N-iter: %s - Energy: %8.3f" % (qs._final_n_iter, qs._final_energy)
    #print quaternions

if __name__ == '__main__':
    main()
