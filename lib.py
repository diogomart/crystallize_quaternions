import numpy as np

def normalize(v):
    v = np.array(v)
    return v / np.sqrt(np.sum(v**2))

def normalize_all(quaternions):
    for i in xrange(len(quaternions)):
        quaternions[i] = normalize(quaternions[i])
    return

def shoemake(u1, u2, u3):
    w = np.sqrt(1 - u1) * np.sin(np.pi*2*u2)
    x = np.sqrt(1 - u1) * np.cos(np.pi*2*u2)
    y = np.sqrt(u1)     * np.sin(np.pi*2*u3)
    z = np.sqrt(u1)     * np.cos(np.pi*2*u3)
    return w, x, y, z

def random_quaternion():
    u1 = np.random.rand()
    u2 = np.random.rand()
    u3 = np.random.rand()
    return shoemake(u1, u2, u3)

def random_axis():
    x = np.random.normal()
    y = np.random.normal()
    z = np.random.normal()
    length = np.sqrt(x**2 + y**2 * z**2)
    return x/length, y/length, z/length

def axisangle_to_q(v, theta):
    v = np.array(v)
    v = normalize(v)
    x, y, z = v
    theta /= 2.
    w = np.cos(theta)
    x = x * np.sin(theta)
    y = y * np.sin(theta)
    z = z * np.sin(theta)
    return w, x, y, z

def q_mult(q1, q2):
    w1, x1, y1, z1 = q1
    w2, x2, y2, z2 = q2
    w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
    x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
    y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
    z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
    return w, x, y, z

def angle_vector(v1, v2):
    length1 = np.sqrt(np.dot(v1, v1))
    length2 = np.sqrt(np.dot(v2, v2))
    return np.arccos(np.dot(v1, v2) / (length1 * length2))

def potential(angle_radians, x0=0.0, sigma=0.4):
    """ """

    # linear potential
    return max(0.0, 1.0 - angle_radians / sigma)

    ## gaussian potential
    #return np.exp(-0.5 * (angle_radians - x0)**2 / sigma**2)

def eval_all(quaternions):
    """ some numpy magic would be nice here """
    energy = 0.0
    for i in xrange(len(quaternions)):
        for j in xrange(i+1, len(quaternions)):
            angle = angle_vector(quaternions[i], quaternions[j])
            energy += potential(angle)
    return energy
    
def eval_one(i, quaternions):
    """ some numpy magic would be nice here """
    energy = 0.0
    for j in xrange(len(quaternions)):
        if j == i:
            continue
        angle = angle_vector(quaternions[i], quaternions[j])
        energy += potential(angle)
    return energy

def quaternion_to_shoemake(wxyz, wrap=True):
    """ 
        graphical solution:
          - y and z define a point within a 2D circle of radius 1.0, where
            sqrt(u1) is the distance from (0,0,0). Therefore, u1 is
            y**2 + z**2.
    """

    if abs(np.sum(np.array(wxyz)**2) - 1.0) > 1e-15:
        raise RuntimeError('input quaternion is not normalized')

    w, x, y, z = wxyz

    u1 = y**2 + z**2
    u1_ = -(w**2 + x**2) + 1

    if abs(u1 - u1_) > 1e-15:
        raise RuntimeError('expected u1 to be equal to u1_')

    if u1 == 0.:
        print "WARNING: numerical singularity in quaternion_to_cube3"
        print "WARNING: impossible to determine u3"

    if u1 == 1.:
        print "WARNING: numerical singularity in quaternion_to_cube3"
        print "WARNING: impossible to determine u2"

    # arctan2 returns values [-pi, +pi]
    u2 = np.arctan2(w,x)
    u3 = np.arctan2(y,z)

    if not wrap:
        return u1, u2, u3

    if u2 < 0.:
        u2 += 2.0 * np.pi
    if u3 < 0.:
        u3 += 2.0 * np.pi

    return u1, u2, u3

