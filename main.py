#!/usr/bin/env python

from lib import *
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt

n_quaternions = 200
mc_steps = 10000
report_steps = 100
#temperature = 1.2

# quaternions are perturbed by multiplying them with another quaternion (q2)
# q2 shall correspond to a small rotation (e.g. up to 15 deg or so)
# the axis shall be chosen uniformely from the sphere surface
# the small rotation shall be sampled from a gaussian distribution
perturbation_scale = 0.1

# initialize system
quaternions = []
for _ in xrange(n_quaternions):
    quaternions.append(random_quaternion())

u1 = []
u2 = []
u3 = []
# visualize population in shoemake space
for q in quaternions:
    a, b, c = quaternion_to_shoemake(q)
    u1.append(a)
    u2.append(b)
    u3.append(c)

ax = plt.gca(projection='3d')
ax.plot(u1, u2, u3, 'ok')
plt.show()

# monte carlo
for i in xrange(mc_steps):

    if i % report_steps == 0:
        normalize_all(quaternions)
        total_energy = eval_all(quaternions)
        print "iteration number %10d --- total energy: %15.6f" % (i, total_energy)

    # random perturbation
    rotaxis = random_axis()
    rotangle = perturbation_scale * np.random.normal()
    q2 = axisangle_to_q(rotaxis, rotangle)

    # choose one quaternion randomly
    q_idx = np.random.choice(n_quaternions)
    q1 = quaternions[q_idx]

    # evaluate energy involving the chosen quaternion
    e0 = eval_one(q_idx, quaternions)
    quaternions[q_idx] = q_mult(quaternions[q_idx], q2)

    # evaluate energy change
    e1 = eval_one(q_idx, quaternions)

    # replace by metropolis criterion
    if e1 > e0:
        quaternions[q_idx] = q1
    
u1 = []
u2 = []
u3 = []
# visualize population in shoemake space
for q in quaternions:
    a, b, c = quaternion_to_shoemake(q)
    u1.append(a)
    u2.append(b)
    u3.append(c)

ax = plt.gca(projection='3d')
ax.plot(u1, u2, u3, 'ok')
plt.show()

